package Analyzer;

import CSV.WorkerCSV;
import Config.Config;
import ErrorLogs.Logger;
import Structures.Result;
import Structures.Task;
import Taxi.Maxim;
import Taxi.Yandex;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Recv {


    public static void main(String[] argv){
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        Logger logger = new Logger();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String d = format.format(date);
        int id = 1;
        Config config = new Config();
        int waitTime = 0;
        try {
            waitTime = config.timeConfig();
        } catch (Exception e) {
            logger.logWriter(e);
        }
        Task task = taskReader(id);

        while(true) {
            while (task != null) {

                Integer costY = null, costM = null;

                try {
                    costY = Yandex.getYandexCost(task.getFrom(), task.getTo());
//                    System.out.println(costY);
                } catch (InterruptedException e) {
                    logger.logWriter(e);
                }



                String[] from = parser(task.getFrom());
                String[] to = parser(task.getTo());
                try {
                    costM = Maxim.getMaximCost(from, to);
                } catch (InterruptedException e) {
                    logger.logWriter(e);
                    continue;
                }

                Result result = new Result(task.getId(), costY, costM, task.getFrom(), task.getTo(), task.getDistance(), d);
                taskWriter(result);
//                System.out.println(costM);

                id++;
                task = taskReader(id);
            }
            System.out.println("Waitting:" + waitTime);

            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException e) {
                logger.logWriter(e);
            }
        }

    }

    private static String[] parser(String s){

        String[] address = new String[2];
        String homeNum = "";
        String tmp = "";
        int i = s.length()-1;

        while(s.charAt(i) != ' '){
            tmp += s.charAt(i);
            i--;
        }

        for(int j = tmp.length() - 1; j >= 0; j--){
            homeNum += tmp.charAt(j);
        }

        s = s.replaceAll(homeNum, "");
        address[0] = s;
        address[1] = homeNum;

        return address;
    }

    private static Task taskReader(int id){
        WorkerCSV workerCSV = new WorkerCSV();
        Task task = new Task();

        String[] t = workerCSV.readerCSV(id);

        if(t == null)return null;

        task.setFrom(t[0]);
        task.setTo(t[1]);
        task.setId(id);
        task.setDistance(t[2]);

        return task;
    }

    private static void taskWriter(Result result){
        WorkerCSV workerCSV = new WorkerCSV();
        workerCSV.writeCSV(result);
    }
}
