package Structures;

public class Result {
    private long taskID;
    private int priceYandex;
    private int priceMaxim;
    private String from;
    private String to;
    private String distance;
    private String date;

    public Result(long taskID, int priceYandex, int priceMaxim, String from, String to, String distance, String date) {
        this.taskID = taskID;
        this.priceYandex = priceYandex;
        this.priceMaxim = priceMaxim;
        this.from = from;
        this.to = to;
        this.distance = distance;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getPriceMaxim() {
        return priceMaxim;
    }

    public void setPriceMaxim(int priceMaxim) {
        this.priceMaxim = priceMaxim;
    }

    public long getTaskID() {
        return taskID;
    }

    public void setTaskID(long taskID) {
        this.taskID = taskID;
    }

    public int getPriceYandex() {
        return priceYandex;
    }

    public void setPriceYandex(int priceYandex) {
        this.priceYandex = priceYandex;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return  date +
                "    " +taskID +
                ", Yandex: " + priceYandex +
                ", Maxim: " + priceMaxim +
                "," + from +
                "," + to  +
                "," + distance;
    }
}
