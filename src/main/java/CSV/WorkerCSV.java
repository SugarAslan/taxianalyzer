package CSV;

import ErrorLogs.Logger;
import Structures.Result;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class WorkerCSV {
    private Logger logger = new Logger();

    public String[] readerCSV(int i){


        FileReader file = null;
        try {
            file = new FileReader("input.csv");
        } catch (FileNotFoundException e) {
            logger.logWriter(e);
        }

        CSVReader csvReader = new CSVReader(file, ',', '"', i);

        String[] nextLine;
        try {
            nextLine = csvReader.readNext();
            if (nextLine != null) return nextLine;
        } catch (IOException e) {
            logger.logWriter(e);
        }
        return null;

    }

    public String[] readerCSV(){


        FileReader file = null;
        try {
            file = new FileReader("config.csv");
        } catch (FileNotFoundException e) {
            logger.logWriter(e);
        }

        CSVReader csvReader = new CSVReader(file, ',', '"', 1);

        String[] nextLine;
        try {
            nextLine = csvReader.readNext();
            if (nextLine != null) return nextLine;
        } catch (IOException e) {
            logger.logWriter(e);

        }

        return null;

    }

    public void writeCSV(Result result){

        FileWriter file = null;
        try {
            file = new FileWriter("output.csv", true);
        } catch (IOException e) {
            logger.logWriter(e);
        }
        CSVWriter writer = new CSVWriter(file);
        String[] record = result.toString().split(",");
        writer.writeNext(record);
        try {
            writer.close();
        } catch (IOException e) {
            logger.logWriter(e);
        }
    }

}
