package ErrorLogs;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    public void logWriter(Exception ex){

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String d = format.format(date);
        String[] log = new String[2];
        FileWriter file = null;

        log[0] = d;
        log[1] = ex.toString();

        try {
            file = new FileWriter("logs.csv", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVWriter writer = new CSVWriter(file);
        writer.writeNext(log);
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
