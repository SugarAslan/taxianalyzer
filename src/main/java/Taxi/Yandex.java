package Taxi;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.openqa.selenium.Keys.*;

//"улица Штыба, 6/33"
//"улица Хаджи Мамсурова, 42"

public class Yandex {

    static int timeOut = 2000;

    public static Integer getYandexCost(String from, String to) throws InterruptedException {

        int cost = -1;
        int tik = 0;
        String tmp = null;
        boolean f = false;

        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);

//        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("start-maximized");
        options.addArguments("disable-infobars");
        options.addArguments("--disable-extensions");

        WebDriver webDriver = new ChromeDriver(options);

        webDriver.navigate().to("https://taxi.yandex.ru/");

//        System.out.println(webDriver.getCurrentUrl());

        webDriver.findElement(By.id("addressFrom")).clear();
        Thread.sleep(timeOut);

        webDriver.findElement(By.id("addressFrom")).sendKeys(from);
        Thread.sleep(timeOut);

        webDriver.findElement(By.id("addressTo")).sendKeys(to);
        Thread.sleep(timeOut);
        webDriver.findElement(By.id("addressTo")).sendKeys(ARROW_DOWN);
        webDriver.findElement(By.id("addressTo")).sendKeys(ENTER);
        Thread.sleep(1100);

        tmp = webDriver.findElement(By.className("text")).getText();

        while (cost == -1){

            tmp = copy(tmp);

            try {
                cost = Integer.valueOf(tmp);

            } catch (NumberFormatException e) {
//                System.out.println(cost + " is not a number");
                Thread.sleep(200);

                if(tik % 10 == 0){
                    webDriver.findElement(By.id("addressFrom")).click();
                    Thread.sleep(1500);
                }
                if(tik == 30){cost = getYandexCost(from, to);}

            }

            if(cost == -1)tmp = request(webDriver);
            tik++;
        }


                webDriver.close();
                webDriver.quit();

            return  cost;

    }

    static String request(WebDriver webDriver){

        return webDriver.findElement(By.className("text")).getText();

    }

    static String copy(String str){

        String tmp = "";

        for (int i = 0; i < str.length()-2; i++) {
            tmp += str.charAt(i);
        }

        return tmp;
    }
}
