package Taxi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.openqa.selenium.Keys.ARROW_DOWN;
import static org.openqa.selenium.Keys.ENTER;

public class Maxim {

    static int timeOut = 1000;

    public static Integer getMaximCost (String[] from, String[] to) throws InterruptedException {
        int cost = -1;
        int tik = 0;
        String tmp = "";

        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);

//        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("start-maximized");
        options.addArguments("disable-infobars");
        options.addArguments("--disable-extensions");


        WebDriver webDriver = new ChromeDriver(options);

        webDriver.navigate().to("https://taximaxim.ru/?city=846-Владикавказ&referrer=self");

//        System.out.println(webDriver.getCurrentUrl());

        webDriver.switchTo().frame("order-form-frame");
        webDriver.findElement(By.id("addressform-0-pointfield")).sendKeys(from[0]);
        Thread.sleep(timeOut);

        webDriver.findElement(By.id("addressform-0-pointfield")).sendKeys(ARROW_DOWN);
        webDriver.findElement(By.id("addressform-0-pointfield")).sendKeys(ENTER);
        Thread.sleep(timeOut);

        webDriver.findElement(By.id("addressform-0-house")).sendKeys(from[1]);
        webDriver.findElement(By.id("addressform-0-rem")).sendKeys("999");
        Thread.sleep(timeOut);

        webDriver.findElement(By.id("addressform-1-pointfield")).sendKeys(to[0]);
        Thread.sleep(timeOut);
        webDriver.findElement(By.id("addressform-1-pointfield")).sendKeys(ARROW_DOWN);
        webDriver.findElement(By.id("addressform-1-pointfield")).sendKeys(ENTER);
        Thread.sleep(1100);

        webDriver.findElement(By.id("addressform-1-house")).sendKeys(to[1]);
        Thread.sleep(1200);


        while (cost == -1){

            tmp = copy(tmp);

            try {
                cost = Integer.valueOf(tmp);

            } catch (NumberFormatException e) {
//                System.out.println(cost + " is not a number");
                Thread.sleep(200);

                if(tik == 10){
                    Thread.sleep(1500);
                }
                if(tik == 15){
                    cost = getMaximCost(from, to);
                }
            }

            if(cost == -1)tmp = request(webDriver);

            tik++;
        }

        webDriver.close();
        webDriver.quit();

        return  cost;

    }

    static String request(WebDriver webDriver){

        return webDriver.findElement(By.id("price")).getText();

    }

    static String copy(String str){

        String tmp = "";

        for (int i = 0; i < str.length()-2; i++) {
            tmp += str.charAt(i);
        }

        return tmp;
    }
}
